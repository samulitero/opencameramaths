using System;
using System.Runtime.InteropServices;

namespace BackendExample
{
    class BackendTester
    {

        [DllImport("libocm.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int Sanity1(
            int numbar,
            [MarshalAs(UnmanagedType.LPArray)] byte[] string_filled_in_dll
            );

        static void Main(string[] args)
        {
            Console.WriteLine("Press any key to test the dll");
            Console.ReadKey();
            TestDll();
            Console.ReadKey();
        }

        private static void TestDll()
        {
            byte[] string_filled_in_dll = new byte[21];
            int ret = Sanity1(5, string_filled_in_dll);

            Console.WriteLine("Return Value: " + ret);
            Console.WriteLine("String filled in DLL: " + System.Text.Encoding.ASCII.GetString(string_filled_in_dll));
        }
    }
}
