﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorAspNetInterface.Controllers.ImageProcessing
{
    public class Response
    {
        public string question;
        public string result;

        public Response(string question, string result)
        {
            this.question = question;
            this.result = result;
        }
    }
}
