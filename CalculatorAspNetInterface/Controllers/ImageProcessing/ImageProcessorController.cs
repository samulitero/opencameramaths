using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CalculatorAspNetInterface.Controllers.ImageProcessing
{
    
    [Route("api/[controller]")]
    public class ImageProcessorController : Controller
    {
        public class ImageData
        {
            public string img;
        }


        [HttpPost("[action]")]
        public IEnumerable<Response> ProcessImage([FromBody] ImageData imageData)
        {
            byte[] imageBytes;
            try
            {
                imageBytes = Convert.FromBase64String(imageData.img);
            }
            catch (FormatException)
            {
                throw new Exception("image format was not satisfactory!");
            }

            Response[] responses = null;

            if (Environment.Is64BitOperatingSystem)
            {
                responses= LibraryConnection.ProcessImage(imageBytes);
            }

            if(responses == null || responses.Length == 0)
            {
                throw new Exception("Image processing failed");
            }


            return responses;
        }
        
    }
}
