﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace CalculatorAspNetInterface.Controllers.ImageProcessing
{
    public class LibraryConnection
    {

        [DllImport(@"libocm.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int _Z6SanityiPc(
            int numbar,
            [MarshalAs(UnmanagedType.LPArray)] byte[] string_filled_in_dll
            );


        public static Response[] ProcessImage(byte[] image)
        {
            byte[] string_filled_in_dll = new byte[21];
            int ret = _Z6SanityiPc(5, string_filled_in_dll);

            return new Response[] { new Response("2+2", ret.ToString()) };
        }

    }
}
