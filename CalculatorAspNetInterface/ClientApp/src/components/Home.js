import React from 'react';
import { connect } from 'react-redux';
import CameraView from './CameraView';

const Home = props => (
  <div>
        <h1>Hello, world!</h1>
        <CameraView></CameraView>
  </div>
);

export default connect()(Home);
