﻿import React, { Component } from 'react';

class ResultShowcase extends Component {
    constructor(props) {
        super(props);

        this.state = {
            calculation: props.calculation
        };
    }


    render() {
        return (
            <p> Result for {this.state.calculation.question} was {this.state.calculation.result} </p>
)
    }

}

export default ResultShowcase;