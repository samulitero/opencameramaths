﻿import React, { Component } from 'react';
import ResultShowcase from './ResultShowcase';
import CameraWindow from './CameraWindow';

class CameraView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cameraStream: null,
            latestPicture: null,
            latestCalculation: null,
            errorMessage: null
        };
    }

    static hasGetUserMedia() {
        return !!(navigator.mediaDevices &&
            navigator.mediaDevices.getUserMedia);
    }


    registerStream() {
        const constraints = {
            video: true,
            audio: false
        };

        navigator.mediaDevices.getUserMedia(constraints).
            then((stream) => {
                this.setState(() => {
                    return { cameraStream: stream };
                });
            });
    }


    componentWillMount() {
        if (CameraView.hasGetUserMedia) {
            this.registerStream();
        }
        else {
            console.log('no getUserMedia available!');
        }
    }

    drawOnCanvas(source, success, error) {
        let canvas = this.refs.canvasComponent;

        if (canvas) {
            canvas.width = source.getVideoWidth();
            canvas.height = source.getVideoHeight();

            //if (canvas.width === 0 || canvas.height === 0) {
            //    error();
            //    return;
            //}
            canvas.getContext('2d').drawImage(source.getVideo(), 0, 0, canvas.width, canvas.height);

            success(canvas.toDataURL());
            return;
        }

        error();
    }


    processDrawnImage(imgDataUrl) {
        let jsonDataString = JSON.stringify({
            img: imgDataUrl.replace('data:image/png;base64,', '')
        });

        this.setState((state, props) => {
            return { latestPicture: imgDataUrl }
        })

        fetch('api/ImageProcessor/ProcessImage', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: jsonDataString
        }).then((response) => {
            response.json().then(this.displayAnswer.bind(this));
        },
            (err) => {
                this.setErrorMessage('JSON-data could not be read correctly!');
            }
        );
    }


    setErrorMessage(msg) {
        this.setState(() => {
            return { errorMessage: msg };
        });
    }


    onUpdateClicked() {
        if (this.state.cameraStream) {
            this.drawOnCanvas(this.refs.CameraWindow, this.processDrawnImage.bind(this), this.setErrorMessage.bind(this, 'Draw call could not be handled on front'));
        }
        else {
            this.setErrorMessage('Camera Stream is not available!');
        }
    }


    displayAnswer(response) {
        if (response.length < 1) {
            console.log('response should have been longer than 1');
        }

        this.setErrorMessage(null);

        this.setState((state, props) => {
            return { latestCalculation: response[0] }
        });   
    }


    renderErrorMessage(errorMsg) {
        return (
            <div className="alert alert-danger">
                <p> {errorMsg} </p>
            </div>
)
    }


    renderCameraDisplay() {
        const canvasHTMLComponent = <canvas ref="canvasComponent"></canvas>

        const resultShowcase = this.state.latestCalculation ? <ResultShowcase calculation={this.state.latestCalculation} > </ResultShowcase> : null;

        const errorMessage = this.state.errorMessage ? this.renderErrorMessage(this.state.errorMessage) : null;

        return (
            <div>
                <CameraWindow cameraStream={this.state.cameraStream} ref="CameraWindow"></CameraWindow>
                {canvasHTMLComponent}
                {errorMessage}
                {resultShowcase}
            </div>
        );
    }


    render() {
        const camera = CameraView.hasGetUserMedia() ? this.renderCameraDisplay() : <p> Camera not supported on browser! </p>;

        return (
            <React.Fragment>
                {camera}
                <button onClick={this.onUpdateClicked.bind(this)}>Update</button>
            </React.Fragment>
        );
    }
}


export default CameraView;
