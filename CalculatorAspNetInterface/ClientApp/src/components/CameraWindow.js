﻿import React, { Component } from 'react';


export default class CameraWindow extends Component {

    constructor(props) {
        super(props);

        this.videoPlayer = React.createRef();
    }
    
 
    getVideoWidth() {
        if (this.videoPlayer) {
            return this.videoPlayer.current.videoWidth;
        }
        return this.props.videoWidth || 640;
    }


    getVideoHeight() {
        if (this.videoPlayer) {
            return this.videoPlayer.current.videoHeight;
        }
        return this.props.videoHeight || 480;
    }

    getVideo() {
        return this.videoPlayer.current;
    }


    render() {
        let videoComponent =
            <video ref={this.videoPlayer} className="dark-background" autoPlay width="auto" height="auto"></video>

        if (this.videoPlayer && this.props.cameraStream) {
            this.videoPlayer.current.srcObject = this.props.cameraStream;
            console.log(this.props.cameraStream);
        }

        return (
            <React.Fragment>
                {videoComponent}
            </React.Fragment>
        );
    }
}