#ifndef OCM_LIBRARY_H
#define OCM_LIBRARY_H

struct MATH_RESULT {
   char* calculation;
   char* result;
   int bounds_max_x;
   int bounds_max_y;
   int bounds_min_x;
   int bounds_min_y;
};

//#define DllExport extern "C" __declspec( dllexport )
extern "C"{
   __declspec( dllexport ) int Sanity1(int a, char* b); 
}
/*
DllExport MATH_RESULT Analyze(std::byte* byteImage);
DllExport MATH_RESULT AnalyzeFile(char* path);
cv::Mat BytesToMat(std::byte* byteImage);
*/

#endif // OCM_LIBRARY_H